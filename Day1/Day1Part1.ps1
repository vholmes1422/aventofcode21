$Start = (Get-Date).Millisecond
[int[]]$data = Get-Content .\Day1\Part1input.txt

    $Count = 0
    For ($i = 0; $i -le $data.count; $i++)
    {
        If(($data[$i+1] - $data[$i]) -gt 0){
        $Count++
      }
     }
Write-host "The answer is $Count"
$End = (Get-Date).Millisecond
Write-Host "The script took $($Start - $End)ms"
